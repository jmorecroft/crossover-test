import * as Effect from "@effect/io/Effect";
import axios from "axios";
import { pipe } from "@effect/data/Function";

interface ChargeArgs {
  serviceType: string;
  unit: number;
}

interface ChargeResult {
  remainingBalance: number;
  charges: number;
  isAuthorized: boolean;
  balanceCheckDuration: number;
}

const resetRedis = (): Effect.Effect<never, Error, number> =>
  pipe(
    Effect.tryCatchPromise(
      () =>
        axios.post(
          "https://o1c1vi2h9h.execute-api.us-east-1.amazonaws.com/prod/reset-redis"
        ),
      (e) => e as Error
    ),
    Effect.map((result) => result.data as number)
  );

const chargeRedis = (
  data: ChargeArgs
): Effect.Effect<never, Error, ChargeResult> =>
  pipe(
    Effect.tryCatchPromise(
      () =>
        axios.post(
          "https://o1c1vi2h9h.execute-api.us-east-1.amazonaws.com/prod/charge-request-redis",
          data
        ),
      (e) => e as Error
    ),
    Effect.map((result) => result.data)
  );

const resetMemcached = (): Effect.Effect<never, Error, number> =>
  pipe(
    Effect.tryCatchPromise(
      () =>
        axios.post(
          "https://lg3bqs4lj6.execute-api.us-east-1.amazonaws.com/prod/reset-memcached"
        ),
      (e) => e as Error
    ),
    Effect.map((result) => result.data as number)
  );

const chargeMemcached = (
  data: ChargeArgs
): Effect.Effect<never, Error, ChargeResult> =>
  pipe(
    Effect.tryCatchPromise(
      () =>
        axios.post(
          "https://lg3bqs4lj6.execute-api.us-east-1.amazonaws.com/prod/charge-request-memcached",
          data
        ),
      (e) => e as Error
    ),
    Effect.map((result) => result.data)
  );

const perfTest = (
  charge: (args: ChargeArgs) => Effect.Effect<never, Error, ChargeResult>,
  reset: () => Effect.Effect<never, Error, number>
) =>
  pipe(
    charge({ serviceType: "perf", unit: 1 }),
    Effect.tap((r) => (!r.isAuthorized ? reset() : Effect.succeed(0))),
    Effect.map((r) => r.balanceCheckDuration)
  );

const functionalTest = (
  charge: (args: ChargeArgs) => Effect.Effect<never, Error, ChargeResult>,
  reset: () => Effect.Effect<never, Error, number>
) =>
  Effect.gen(function* ($) {
    yield* $(reset());

    const [res1, res2] = yield* $(
      Effect.allPar([
        charge({ serviceType: "func", unit: 1 }),
        charge({ serviceType: "func", unit: 1 }),
      ])
    );

    const authed = res1.isAuthorized || res2.isAuthorized;
    const failed = !res1.isAuthorized || !res2.isAuthorized;
    const charges = res1.charges + res2.charges;
    const balance = res1.remainingBalance + res2.remainingBalance;

    if (!authed || !failed || charges !== 100 || balance !== 0) {
      yield* $(
        Effect.fail(
          `failed functional tests: res1: ${JSON.stringify(
            res1,
            null,
            "\n"
          )}, res2: ${JSON.stringify(res2, null, "\n")}`
        )
      );
    }
  });

const runPerfTests = (
  count: number,
  charge: (args: ChargeArgs) => Effect.Effect<never, Error, ChargeResult>,
  reset: () => Effect.Effect<never, Error, number>
) =>
  pipe(
    Effect.replicateEffect(perfTest(charge, reset), count),
    Effect.tap(() => reset()),
    Effect.map((millis) => ({
      avgBalanceCheck: millis.reduce((s, n) => s + n, 0) / count,
      maxBalanceCheck: millis.reduce((m, n) => Math.max(m, n), 0),
    })),
    Effect.tap(({ avgBalanceCheck, maxBalanceCheck }) =>
      Effect.log(
        `charge calls ${count}, avg,max balance check: ${avgBalanceCheck}ms, ${maxBalanceCheck}ms`
      )
    )
  );

const runFunctionalTests = (
  count: number,
  charge: (args: ChargeArgs) => Effect.Effect<never, Error, ChargeResult>,
  reset: () => Effect.Effect<never, Error, number>
) => Effect.replicateEffect(functionalTest(charge, reset), count);

const testRedisPerf = runPerfTests(10, chargeRedis, resetRedis);
const testMemcachedPerf = runPerfTests(10, chargeMemcached, resetMemcached);
const testRedisFunctional = runFunctionalTests(10, chargeRedis, resetRedis);
const testMemcachedFunctional = runFunctionalTests(
  10,
  chargeMemcached,
  resetMemcached
);

const program = pipe(
  Effect.log("testing redis perf"),
  Effect.tap(() => testRedisPerf),
  Effect.tap(() => Effect.log("testing memcached perf")),
  Effect.tap(() => testMemcachedPerf),
  Effect.tap(() => Effect.log("testing redis functional")),
  Effect.tap(() =>
    pipe(
      testRedisFunctional,
      Effect.tapError(() => Effect.log("failed redis functional, expected")),
      Effect.catchAll(() => Effect.unit())
    )
  ),
  Effect.tap(() => Effect.log("testing memcached functional")),
  Effect.tap(() => testMemcachedFunctional)
);

Effect.runPromise(program).then(() => console.log("done"));
