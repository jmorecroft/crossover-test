"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redis = require("redis");
const memcached = require("memcached");
const util = require("util");
const KEY = `account1/balance`;
const DEFAULT_BALANCE = 100;
const CHARGES = 100;
const MAX_EXPIRATION = 60 * 60 * 24 * 30;
const memcachedClient = new memcached(
  `${process.env.ENDPOINT}:${process.env.PORT}`
);
exports.chargeRequestRedis = async function (input) {
  const redisClient = await getRedisClient();
  var beforeCheck = Date.now();
  var remainingBalance = await getBalanceRedis(redisClient, KEY);
  var charges = getCharges();
  const isAuthorized = authorizeRequest(remainingBalance, charges);
  var afterCheck = Date.now();
  var balanceCheckDuration = afterCheck - beforeCheck;

  if (!isAuthorized) {
    await disconnectRedis(redisClient);

    return {
      remainingBalance,
      isAuthorized,
      charges: 0,
      balanceCheckDuration,
    };
  }
  remainingBalance = await chargeRedis(redisClient, KEY, charges);
  await disconnectRedis(redisClient);
  return {
    remainingBalance,
    charges,
    isAuthorized,
    balanceCheckDuration,
  };
};
exports.resetRedis = async function () {
  const redisClient = await getRedisClient();
  const ret = new Promise((resolve, reject) => {
    redisClient.set(KEY, String(DEFAULT_BALANCE), (err, res) => {
      if (err) {
        reject(err);
      } else {
        resolve(DEFAULT_BALANCE);
      }
    });
  });
  await disconnectRedis(redisClient);
  return ret;
};
exports.resetMemcached = async function () {
  var ret = new Promise((resolve, reject) => {
    memcachedClient.set(KEY, DEFAULT_BALANCE, MAX_EXPIRATION, (res, error) => {
      if (error) resolve(res);
      else reject(DEFAULT_BALANCE);
    });
  });
  return ret;
};
exports.chargeRequestMemcached = async function (input) {
  const maxAttempts = 3;
  for (var i = 0; i < maxAttempts; i++) {
    var beforeCheck = Date.now();
    var [remainingBalance, cas] = await getBalanceMemcached(KEY);
    var charges = getCharges();
    var isAuthorized = authorizeRequest(remainingBalance, charges);
    var afterCheck = Date.now();
    var balanceCheckDuration = afterCheck - beforeCheck;

    if (!authorizeRequest(remainingBalance, charges)) {
      return {
        remainingBalance,
        isAuthorized,
        charges: 0,
        balanceCheckDuration,
      };
    }
    var newBalance = remainingBalance - charges;
    var updated = await chargeMemcached(KEY, newBalance, cas);
    if (updated) {
      return {
        remainingBalance: newBalance,
        charges,
        isAuthorized,
        balanceCheckDuration,
      };
    }
  }
  throw new Error("failed to update");
};

async function getRedisClient() {
  return new Promise((resolve, reject) => {
    try {
      const client = new redis.RedisClient({
        host: process.env.ENDPOINT,
        port: parseInt(process.env.PORT || "6379"),
      });
      client.on("ready", () => {
        console.log("redis client ready");
        resolve(client);
      });
    } catch (error) {
      reject(error);
    }
  });
}
async function disconnectRedis(client) {
  return new Promise((resolve, reject) => {
    client.quit((error, res) => {
      if (error) {
        reject(error);
      } else if (res == "OK") {
        console.log("redis client disconnected");
        resolve(res);
      } else {
        reject("unknown error closing redis connection.");
      }
    });
  });
}
function authorizeRequest(remainingBalance, charges) {
  return remainingBalance >= charges;
}
function getCharges() {
  return CHARGES;
}
async function getBalanceRedis(redisClient, key) {
  const res = await util
    .promisify(redisClient.get)
    .bind(redisClient)
    .call(redisClient, key);
  return parseInt(res || "0");
}
async function chargeRedis(redisClient, key, charges) {
  return util
    .promisify(redisClient.decrby)
    .bind(redisClient)
    .call(redisClient, key, charges);
}
async function getBalanceMemcached(key) {
  return new Promise((resolve, reject) => {
    memcachedClient.gets(key, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve([Number(data[key]), data.cas]);
      }
    });
  });
}
async function chargeMemcached(key, newBalance, cas) {
  return new Promise((resolve, reject) => {
    memcachedClient.cas(
      key,
      String(newBalance),
      cas,
      MAX_EXPIRATION,
      (err, updated) => {
        if (err) {
          reject(err);
        } else {
          return resolve(updated);
        }
      }
    );
  });
}
